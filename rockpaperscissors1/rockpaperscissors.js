var result = "";
var player = "";

var resultTable = {
"rock":{"rock":"draw", "paper":"win", "scissors":"lose"},
"paper":{"rock":"lose", "paper":"draw", "scissors":"win"},
"scissors":{"rock":"win", "paper":"lose", "scissors":"draw"},
"lizard":{"rock":"win", "paper":"lose", "scissors":"win"},
"spock":{"rock":"lose", "paper":"win", "scissors":"lose"}
};

var choices = ["rock","paper","scissors"];

do {
    if (player === "") {
        player = prompt("Pick rock, paper, or scissors.");
    } else {
        player = prompt("I said, pick rock, paper, or scissors!");
    }
    if (player===null) {
        result = "Bye!"; break;
    } else {
        player = player.toLowerCase();
    } 
} 
while (player !== choices[0] && player !== choices[1] && player !== choices[2]);

var computer = choices[Math.floor(Math.random()*3)];

var results = {};
results.win = "Your "+player+" beats "+computer+". You win.";
results.lose = "Your "+player+" loses to "+computer+". You lose.";
results.draw = "A draw: "+player+" on "+computer+".";

if (result === "") result = results[resultTable[computer][player]];

console.log(result);
