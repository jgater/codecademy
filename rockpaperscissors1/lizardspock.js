var resultText = "";
var player = "";
var character = "";
var result = "";

var resultTable = {
"rock":{"rock":"matches", "paper":"Covers", "scissors":"blunted", "lizard":"crushed", "spock":"Vaporizes"},
"paper":{"rock":"covered", "paper":"matches", "scissors":"Cut", "lizard":"Eats", "spock":"disproven"},
"scissors":{"rock":"Blunts", "paper":"cut", "scissors":"matches", "lizard":"decapitated", "spock":"Smashes"},
"lizard":{"rock":"Crushes", "paper":"eaten", "scissors":"Decapitates", "lizard":"matches", "spock":"poisoned"},
"spock":{"rock":"vaporized", "paper":"Disproves", "scissors":"smashed", "lizard":"Poisons", "spock":"matches"}
};

var choices = ["rock","paper","scissors","lizard","spock"];

do {
    if (player === "") {
        player = prompt("Pick rock, paper, scissors, lizard or spock.");
    } else {
        player = prompt("I said, pick rock, paper, scissors, lizard or spock!");
    }
    if (player===null) {
        resultText = "Bye!"; break;
    } else {
        player = player.toLowerCase();
    } 
} 
while (player !== choices[0] && player !== choices[1] && player !== choices[2] && player !== choices[3] && player !== choices[4]);

var computer = choices[Math.floor(Math.random()*5)];

if (resultText === "") {
    result = resultTable[computer][player];
    character = result[0].toUpperCase();
    if (result === "matches") {
    resultText = "A draw: Your "+player+" "+result+" computer's "+computer+".";
    }
    else if (result[0]===character) { resultText = "Your "+player+ " " + result.toLowerCase() + " computer's "+computer+". You win."; }
    else {resultText = "Your " + player + " is " + result + " by the computer's " + computer + ". You lose.";}
}

console.log(resultText);